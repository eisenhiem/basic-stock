<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "stock".
 *
 * @property int $stock_id
 * @property string $stock_name
 * @property string|null $stock_manager
 * @property string|null $stock_level
 * @property string|null $is_active
 */
class Stock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stock_name'], 'required'],
            [['stock_name', 'stock_manager'], 'string', 'max' => 255],
            [['stock_level', 'is_active'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'stock_id' => 'เลขที่คลัง',
            'stock_name' => 'ชื่อคลัง',
            'stock_manager' => 'ผู้ดูแลคลัง',
            'stock_level' => 'ระดับคลัง',
            'is_active' => 'สถานะ',
        ];
    }
    
    public static function itemsAlias($key){
        $items = [
            'status' => [
                '1' => 'เปิดใช้งาน',
                '2' => 'ปิดใช้งาน',
            ],
            'level' => [
                '1' => 'คลังหลัก',
                '2' => 'คลังย่อย',
                '3' => 'Sub Stock',
            ],
//            'care' => ArrayHelper::map(Officers::find()->where(['hcode' => Yii::$app->user->identity->profile->hospcode])->andWhere(['officer_role' => 'ผู้ให้การรักษา'])->all(), 'officer_id', 'officer_name'),
//            'hospital' => ArrayHelper::map(Hosp::find()->all(), 'hospname', 'hospname'),
        ];
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemStatus()
    {
        return self::itemsAlias('status');
    }

    public function getItemLevel()
    {
        return self::itemsAlias('level');
    }

    public function getStatusName(){
        return ArrayHelper::getValue($this->getItemStatus(),$this->is_active);
    }

    public function getLevelName(){
        return ArrayHelper::getValue($this->getItemLevel(),$this->stock_level);
    }
}
