<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property int $user_id
 * @property int|null $stock_id
 * @property string|null $fullname
 * @property string|null $cid
 * @property string|null $position
 * @property string|null $position_level
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'stock_id'], 'integer'],
            [['fullname'], 'string', 'max' => 255],
            [['cid'], 'string', 'max' => 13],
            [['position'], 'string', 'max' => 100],
            [['position_level'], 'string', 'max' => 30],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'stock_id' => 'Stock ID',
            'fullname' => 'Fullname',
            'cid' => 'Cid',
            'position' => 'Position',
            'position_level' => 'Position Level',
        ];
    }
}
