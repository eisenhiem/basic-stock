<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "despend".
 *
 * @property int $despend_id
 * @property int $stock_id
 * @property int $item_id
 * @property string $despend_date
 * @property int $qty
 * @property int|null $to_stock
 * @property int|null $req_id
 * @property string|null $status_id
 */
class Despend extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    const PENDING = '1';
    const COMPLETE = '2';
    const DONE = '3';
    const INCOMPLETE = '4';
    const RECIEVE = '5';

    public static function tableName()
    {
        return 'despend';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stock_id', 'item_id', 'despend_date', 'qty'], 'required'],
            [['stock_id', 'item_id', 'qty', 'to_stock', 'req_id'], 'integer'],
            [['despend_date'], 'safe'],
            [['status_id'],'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'despend_id' => 'ตัดจ่ายเลขที่',
            'stock_id' => 'คลัง',
            'item_id' => 'รายการ',
            'despend_date' => 'วันที่ตัดจ่าย',
            'qty' => 'จำนวน',
            'to_stock' => 'ตัดจ่ายไปที่',
            'req_id' => 'เลขที่รายการขอเบิก',
            'status_id' => 'สถานะตัดจ่าย',
        ];
    }

    public function getItem()
    {
        return $this->hasOne(Item::className(), ['item_id' => 'item_id']);
    }

    public function getStock()
    {
        return $this->hasOne(Stock::className(), ['stock_id' => 'stock_id']);
    }
       
    public function getTostock()
    {
        return $this->hasOne(Stock::className(), ['stock_id' => 'to_stock']);
    }

    public function getToStockName()
    {
        return $this->to_stock == 0 ? 'ตัดจ่ายภายในหน่วย':$this->tostock->stock_name;
    }

    public static function itemsAlias($key)
    {

        $items = [
            'status' => [
                self::PENDING => 'ขอเบิก',
                self::COMPLETE => 'ตัดจ่ายครบถ้วน',
                self::DONE => 'รับเข้าทัั้งหมด',
                self::INCOMPLETE => 'ตัดจ่ายบางส่วน',
                self::RECIEVE => 'รับเข้าบางส่วน'
            ],
        ];
        return ArrayHelper::getValue($items, $key, []);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemStatus()
    {
        return self::itemsAlias('status');
    }

    public function getStatusName(){
        return ArrayHelper::getValue($this->getItemSex(),$this->status_id);
    }

}
