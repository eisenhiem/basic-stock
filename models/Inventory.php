<?php

namespace app\models;


use Yii;
use DateTime;
/**
 * This is the model class for table "inventory".
 *
 * @property int $inventory_id
 * @property int $item_id
 * @property int $stock_id
 * @property int|null $remain
 * @property int|null $user_id
 * @property string|null $d_update
 */
class Inventory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inventory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'stock_id'], 'required'],
            [['item_id', 'stock_id', 'remain', 'user_id'], 'integer'],
            [['d_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'inventory_id' => 'เลขที่',
            'item_id' => 'รายการ',
            'stock_id' => 'คลัง',
            'remain' => 'คงเหลือ',
            'user_id' => 'ผู้ปรับปรุง',
            'd_update' => 'ปรับปรุงล่าสุด',
        ];
    }

    public function getItem()
    {
        return $this->hasOne(Item::className(), ['item_id' => 'item_id']);
    }

    public function getUser()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }

    public static function getThaiDate($date)
    {
        $month = array("01"=>"ม.ค.","02"=>"ก.พ","03"=>"มี.ค.","04"=>"เม.ย.","05"=>"พ.ค.","06"=>"มิ.ย.","07"=>"ก.ค.","08"=>"ส.ค.","09"=>"ก.ย.","10"=>"ต.ค.","11"=>"พ.ย.","12"=>"ธ.ค.");
        if($date){
            $visit = new DateTime($date);
            $d = $visit->format('j');
            $m = $month[$visit->format('m')];
            $y = $visit->format('Y')+543;
            $result = $d.' '.$m.' '.$y;    
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

}
