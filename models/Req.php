<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "req".
 *
 * @property int $req_id
 * @property int $item_id
 * @property int $stock_id
 * @property string $req_date
 * @property int $qty
 * @property string|null $cause
 * @property int $status_id
 * @property string|null $d_update
 */
class Req extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    const PENDING = '1';
    const COMPLETE = '2';
    const DONE = '3';
    const INCOMPLETE = '4';
    const RECIEVE = '5';

    public static function tableName()
    {
        return 'req';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'stock_id', 'req_date', 'qty', 'status_id'], 'required'],
            [['item_id', 'stock_id', 'qty', 'status_id'], 'integer'],
            [['req_date', 'd_update'], 'safe'],
            [['cause'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'req_id' => 'เลขที่',
            'item_id' => 'รายการ',
            'stock_id' => 'ผู้ขอเบิก',
            'req_date' => 'วันที่ขอเบิก',
            'qty' => 'จำนวน',
            'cause' => 'หมายเหตุ',
            'status_id' => 'สถานะ',
            'd_update' => 'วันที่ปรับปรุง',
        ];
    }

    public function getItem()
    {
        return $this->hasOne(Item::className(), ['item_id' => 'item_id']);
    }

    public function getStock()
    {
        return $this->hasOne(Stock::className(), ['stock_id' => 'stock_id']);
    }

    public static function itemsAlias($key)
    {

        $items = [
            'status' => [
                self::PENDING => 'ขอเบิก',
                self::COMPLETE => 'ตัดจ่ายครบถ้วน',
                self::DONE => 'รับเข้าทัั้งหมด',
                self::INCOMPLETE => 'ตัดจ่ายบางส่วน',
                self::RECIEVE => 'รับเข้าบางส่วน'
            ],
        ];
        return ArrayHelper::getValue($items, $key, []);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemStatus()
    {
        return self::itemsAlias('status');
    }

    public function getStatusName(){
        return ArrayHelper::getValue($this->getItemSex(),$this->status_id);
    }
    
}
