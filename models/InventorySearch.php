<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Inventory;

/**
 * InventorySearch represents the model behind the search form of `app\models\Inventory`.
 */
class InventorySearch extends Inventory
{
    public $item_name;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inventory_id', 'item_id', 'stock_id', 'remain', 'user_id'], 'integer'],
            [['d_update','item_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Inventory::find()->joinWith('item');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'inventory_id' => $this->inventory_id,
            'item_id' => $this->item_id,
            'stock_id' => $this->stock_id,
            'remain' => $this->remain,
            'user_id' => $this->user_id,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'item.item_name', $this->item_name]);

        return $dataProvider;
    }
}
