<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Req;

/**
 * ReqSearch represents the model behind the search form of `app\models\Req`.
 */
class ReqSearch extends Req
{
    public $item_name;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['req_id', 'item_id', 'stock_id', 'qty', 'status_id'], 'integer'],
            [['req_date', 'cause', 'd_update','item_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Req::find()->joinWith('item');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'req_id' => $this->req_id,
            'item_id' => $this->item_id,
            'stock_id' => $this->stock_id,
            'req_date' => $this->req_date,
            'qty' => $this->qty,
            'status_id' => $this->status_id,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'cause', $this->cause])
            ->andFilterWhere(['like', 'item.item_name', $this->item_name]);
            
        return $dataProvider;
    }
}
