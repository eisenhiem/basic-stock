<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AddStock;

/**
 * AddStockSearch represents the model behind the search form of `app\models\AddStock`.
 */
class AddStockSearch extends AddStock
{
    public $item_name;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['add_id', 'stock_id', 'item_id', 'qty', 'user_id'], 'integer'],
            [['add_date','item_name','status_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AddStock::find()->joinWith('item');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'add_id' => $this->add_id,
            'stock_id' => $this->stock_id,
            'item_id' => $this->item_id,
            'add_date' => $this->add_date,
            'qty' => $this->qty,
            'user_id' => $this->user_id,
            'status_id' => $this->status_id,
        ]);
        
        $query->andFilterWhere(['like', 'item.item_name', $this->item_name]);

        return $dataProvider;
    }
}
