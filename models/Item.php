<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item".
 *
 * @property int $item_id
 * @property string $item_name
 * @property string|null $is_active
 * @property string|null $u_update
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_name'], 'required'],
            [['u_update'], 'safe'],
            [['item_name'], 'string', 'max' => 255],
            [['item_unit'], 'string', 'max' => 20],
            [['is_active'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'item_name' => 'ชื่อ',
            'item_unit' => 'หน่วยนับ',
            'is_active' => 'Is Active',
            'u_update' => 'U Update',
        ];
    }
}
