<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Despend;

/**
 * DespendSearch represents the model behind the search form of `app\models\Despend`.
 */
class DespendSearch extends Despend
{
    /**
     * {@inheritdoc}
     */
    public $item_name;

    public function rules()
    {
        return [
            [['despend_id', 'stock_id', 'item_id', 'qty', 'to_stock', 'req_id'], 'integer'],
            [['despend_date','item_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Despend::find()->joinWith('item');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'despend_id' => $this->despend_id,
            'stock_id' => $this->stock_id,
            'item_id' => $this->item_id,
            'despend_date' => $this->despend_date,
            'qty' => $this->qty,
            'to_stock' => $this->to_stock,
            'req_id' => $this->req_id,
        ]);

        $query->andFilterWhere(['like', 'item.item_name', $this->item_name]);

        return $dataProvider;
    }
}
