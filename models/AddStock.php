<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "add_stock".
 *
 * @property int $add_id
 * @property int $stock_id
 * @property int $item_id
 * @property string $add_date
 * @property int $qty
 * @property int $user_id
 * @property string|null $add_date
 */
class AddStock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    const PENDING = '1';
    const COMPLETE = '2';

    public static function tableName()
    {
        return 'add_stock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stock_id', 'item_id', 'add_date', 'qty', 'user_id'], 'required'],
            [['stock_id', 'item_id', 'qty', 'user_id'], 'integer'],
            [['add_date'], 'safe'],
            [['status_id'],'string', 'max' => 1],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'add_id' => 'ลำดับที่',
            'stock_id' => 'คลัง',
            'item_id' => 'รายการ',
            'add_date' => 'วันที่',
            'qty' => 'จำนวน',
            'user_id' => 'ผู้บัทึก',
            'status_id' => 'สถานะการรับเข้าคลัง',
        ];
    }

    public function getItem()
    {
        return $this->hasOne(Item::className(), ['item_id' => 'item_id']);
    }

    public function getStock()
    {
        return $this->hasOne(Stock::className(), ['stock_id' => 'stock_id']);
    }

    public function getUser()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }

    public static function itemsAlias($key)
    {

        $items = [
            'status' => [
                self::PENDING => 'รอรับ',
                self::COMPLETE => 'รับเข้า',
            ],
        ];
        return ArrayHelper::getValue($items, $key, []);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemStatus()
    {
        return self::itemsAlias('status');
    }

    public function getStatusName(){
        return ArrayHelper::getValue($this->getItemSex(),$this->status_id);
    }

}
