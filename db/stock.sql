/*
Navicat MySQL Data Transfer

Source Server         : Local_DB
Source Server Version : 50565
Source Host           : localhost:3306
Source Database       : stock

Target Server Type    : MYSQL
Target Server Version : 50565
File Encoding         : 65001

Date: 2021-08-06 14:59:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `add_stock`
-- ----------------------------
DROP TABLE IF EXISTS `add_stock`;
CREATE TABLE `add_stock` (
`add_id`  int(11) NOT NULL AUTO_INCREMENT ,
`item_id`  int(11) NULL DEFAULT NULL ,
`qty`  int(11) NULL DEFAULT NULL ,
`add_user`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`d_update`  timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY (`add_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of add_stock
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `department`
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
`dep_id`  int(2) NOT NULL AUTO_INCREMENT ,
`dep_name`  varchar(255) CHARACTER SET tis620 COLLATE tis620_thai_ci NULL DEFAULT NULL ,
PRIMARY KEY (`dep_id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=11

;

-- ----------------------------
-- Records of department
-- ----------------------------
BEGIN;
INSERT INTO `department` VALUES ('1', 'กลุ่มงานการพยาบาล'), ('2', 'กลุ่มงานบริหารทั่วไป'), ('3', 'กลุ่มงานทันตกรรม'), ('4', 'องค์กรแพทย์'), ('5', 'กลุ่มงานประกันสุขภาพ ยุทธศาสตร์ฯ'), ('6', 'กลุ่มงานเทคนิคการแพทย์'), ('7', 'กลุ่มงานบริการด้านปฐมภูมิและองค์รวม'), ('8', 'กลุ่มงานเภสัชกรรมและคุ้มครองผู้บริโภค'), ('9', 'กลุ่มงานฟื้นฟูสมรรถภาพ'), ('10', 'กลุ่มงานแพทย์แผนไทยและแพทย์ทางเลือก');
COMMIT;

-- ----------------------------
-- Table structure for `dispend`
-- ----------------------------
DROP TABLE IF EXISTS `dispend`;
CREATE TABLE `dispend` (
`dis_id`  int(11) NOT NULL AUTO_INCREMENT ,
`item_id`  int(11) NULL DEFAULT NULL ,
`qty`  int(11) NULL DEFAULT 0 ,
`dis_user`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`dep_id`  int(11) NULL DEFAULT NULL ,
`d_update`  timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY (`dis_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of dispend
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `item`
-- ----------------------------
DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
`item_id`  int(11) NOT NULL AUTO_INCREMENT ,
`item_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`location_id`  int(11) NULL DEFAULT NULL ,
`qty`  int(11) NOT NULL DEFAULT 0 ,
`use_per_day`  int(11) NULL DEFAULT NULL ,
`minimum`  int(11) NOT NULL DEFAULT 0 ,
`item_type`  varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`d_update`  timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY (`item_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=3

;

-- ----------------------------
-- Records of item
-- ----------------------------
BEGIN;
INSERT INTO `item` VALUES ('1', 'หน้ากาก N95', '10', '12', null, '20', 'UP', '2020-03-27 15:47:07'), ('2', 'หน้ากากอนามัยใยสังเคราะห์ 3 ชั้นขนาด 50ชิ้น/กล่อง', '34', '10', null, '0', null, '2020-03-25 16:48:27');
COMMIT;

-- ----------------------------
-- Table structure for `location`
-- ----------------------------
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
`location_id`  int(3) NOT NULL AUTO_INCREMENT ,
`location_name`  varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`location_id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=44

;

-- ----------------------------
-- Records of location
-- ----------------------------
BEGIN;
INSERT INTO `location` VALUES ('1', 'ลานจอดรถ'), ('2', 'ศาลาพักญาติ'), ('3', 'โรงปั่นไฟฟ้า'), ('4', 'ลานกีฬา'), ('5', 'แฟลต'), ('6', 'บ้านพักแพทย์'), ('7', 'ห้องบัตร'), ('8', 'ห้องฉุกเฉิน'), ('9', 'ห้องตรวจโรค'), ('10', 'ห้องกลุ่มงานบริหารงานทั่วไป'), ('11', 'ห้องเก็บเงิน'), ('12', 'ห้องทันตกรรม'), ('13', 'ห้องเวชและปฐมภูมิ'), ('14', 'ห้อง LAB'), ('15', 'ห้อง X-Ray'), ('16', 'ถนนภายในโรงพยาบาล'), ('17', 'รถ Refer'), ('18', 'ห้องน้ำผู้ป่วยนอก'), ('19', 'ห้องน้ำผู้ป่วยใน'), ('20', 'ห้องน้ำเจ้าหน้าที่'), ('21', 'บริเวรให้บริการผู้ป่วยนอก'), ('22', 'จุดคัดกรอง'), ('23', 'ตึกซักฟอก'), ('24', 'ห้อง Server'), ('25', 'ชุมชน'), ('26', 'ห้องกลุ่มงานประกันฯ'), ('27', 'ห้องคลอด'), ('28', 'ห้องออกกำลังกาย'), ('29', 'ห้องพักพยาบาล ER'), ('30', 'ห้องงานบริการผู้ป่วยใน (Nurse Station)'), ('31', 'ห้องพิเศษ'), ('32', 'ห้องแยกโรค'), ('33', 'ห้องแพทย์แผนไทย'), ('34', 'ห้องจ่ายยา'), ('35', 'คลังยาย่อย'), ('36', 'คลังยา'), ('37', 'ห้องพักแพทย์'), ('38', 'ทางเดินภายในอาคาร'), ('39', 'ห้องเก็บออกซิเจน'), ('40', 'ห้องผู้ป่วยใน'), ('41', 'Social media (Line, Facebook, Intragram, Post website, Blog) '), ('42', 'ร้านค้าร้านอาหารในรพ.'), ('43', 'ห้องเก็บพัสดุ');
COMMIT;

-- ----------------------------
-- Table structure for `status_item`
-- ----------------------------
DROP TABLE IF EXISTS `status_item`;
CREATE TABLE `status_item` (
`status_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`item_id`  int(11) NULL DEFAULT NULL ,
`status_type_id`  int(11) NULL DEFAULT NULL ,
`remark`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`d_update`  timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
`is_active`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' ,
PRIMARY KEY (`status_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=3

;

-- ----------------------------
-- Records of status_item
-- ----------------------------
BEGIN;
INSERT INTO `status_item` VALUES ('1', '1', '1', 'จัดซื้อ 200 ชุด', '2020-03-31 15:12:45', '1'), ('2', '1', '2', 'สนุบสนุน จาก สสจ.100 ชิ้น', '2020-03-31 15:14:30', '0');
COMMIT;

-- ----------------------------
-- Table structure for `status_type`
-- ----------------------------
DROP TABLE IF EXISTS `status_type`;
CREATE TABLE `status_type` (
`status_type_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`status_type_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`status_type_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=4

;

-- ----------------------------
-- Records of status_type
-- ----------------------------
BEGIN;
INSERT INTO `status_type` VALUES ('1', 'จัดซื้อเอง'), ('2', 'ขอสนับสนุน'), ('3', 'รับบริจาค');
COMMIT;

-- ----------------------------
-- Auto increment value for `add_stock`
-- ----------------------------
ALTER TABLE `add_stock` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `department`
-- ----------------------------
ALTER TABLE `department` AUTO_INCREMENT=11;

-- ----------------------------
-- Auto increment value for `dispend`
-- ----------------------------
ALTER TABLE `dispend` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `item`
-- ----------------------------
ALTER TABLE `item` AUTO_INCREMENT=3;

-- ----------------------------
-- Auto increment value for `location`
-- ----------------------------
ALTER TABLE `location` AUTO_INCREMENT=44;

-- ----------------------------
-- Auto increment value for `status_item`
-- ----------------------------
ALTER TABLE `status_item` AUTO_INCREMENT=3;

-- ----------------------------
-- Auto increment value for `status_type`
-- ----------------------------
ALTER TABLE `status_type` AUTO_INCREMENT=4;
