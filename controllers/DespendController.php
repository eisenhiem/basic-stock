<?php

namespace app\controllers;

use app\models\AddStock;
use Yii;
use app\models\Despend;
use app\models\DespendSearch;
use app\models\Req;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Inventory;

/**
 * DespendController implements the CRUD actions for Despend model.
 */
class DespendController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Despend models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DespendSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if(Yii::$app->user->identity->role == 2 ) {
            $dataProvider->query->where(['stock_id' => Yii::$app->user->identity->profile->stock_id]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Despend model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Despend model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($req_id)
    {
        $req = Req::findOne($req_id);

        $model = new Despend();
        $model->stock_id = Yii::$app->user->identity->profile->stock_id;
        if ($req) {
            $model->item_id = $req->item_id;
            $model->to_stock = $req->stock_id;
            $model->req_id = $req->req_id;
            $model->qty = $req->qty;
            $inv = Inventory::find()->where(['item_id' => $model->item_id, 'stock_id' => $model->stock_id])->one();
        }

        if ($model->load(Yii::$app->request->post())) {
            $inv = Inventory::find()->where(['item_id' => $model->item_id, 'stock_id' => $model->stock_id])->one();
            if ($inv->remain >= $model->qty) {
                $model->status_id = '2';
                if ($model->save()) {
                    $inv->remain = $inv->remain - $model->qty;
                    $inv->save();

                    $add = new AddStock();
                    $add->stock_id = $model->to_stock;
                    $add->item_id = $model->item_id;
                    $add->add_date = $model->despend_date;
                    $add->user_id = Yii::$app->user->identity->id;
                    $add->qty = $model->qty;
                    $add->status_id = '1';
                    $add->save();

                    return $this->redirect(['req/index']);
                }
            } else {
                $model->qty = $inv->remain;
                $model->status_id = '4';
                if ($model->save()) {
                    $inv->remain = 0;
                    $inv->save();

                    $add = new AddStock();
                    $add->stock_id = $model->to_stock;
                    $add->item_id = $model->item_id;
                    $add->add_date = $model->despend_date;
                    $add->user_id = Yii::$app->user->identity->id;
                    $add->qty = $model->qty;
                    $add->status_id = '1';
                    $add->save();

                    return $this->redirect(['req/index']);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'inv' => $inv,
        ]);
    }

    public function actionDispend($inv_id)
    {
        $inv = Inventory::findOne($inv_id);

        $model = new Despend();
        $model->stock_id = Yii::$app->user->identity->profile->stock_id;
        $model->item_id = $inv->item_id;
        $model->to_stock = 0;
        $model->req_id = 0;

        if ($model->load(Yii::$app->request->post())) {
            if ($inv->remain >= $model->qty) {
                $model->status_id = '2';
                if ($model->save()) {
                    $inv->remain = $inv->remain - $model->qty;
                    $inv->save();

                    return $this->redirect(['inventory/index']);
                }
            } else {
                $model->qty = $inv->remain;
                $model->status_id = '4';
                if ($model->save()) {
                    $inv->remain = 0;
                    $inv->save();

                    return $this->redirect(['inventory/index']);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'inv' => $inv,
        ]);
    }


    /**
     * Updates an existing Despend model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->despend_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Despend model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Despend model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Despend the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Despend::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
