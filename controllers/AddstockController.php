<?php

namespace app\controllers;

use Yii;
use app\models\AddStock;
use app\models\AddStockSearch;
use app\models\Inventory;
use IntlChar;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AddstockController implements the CRUD actions for AddStock model.
 */
class AddstockController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AddStock models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AddStockSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if(Yii::$app->user->identity->role == 2 ) {
            $dataProvider->query->where(['stock_id' => Yii::$app->user->identity->profile->stock_id]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AddStock model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AddStock model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($inv_id)
    {
        $inv = Inventory::findOne($inv_id);
        $model = new AddStock();
        $model->item_id = $inv->item_id;
        $model->user_id = Yii::$app->user->identity->id;
        $model->stock_id = $inv->stock_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $inv->remain = $inv->remain + $model->qty;
            $inv->save();
            return $this->redirect(['inventory/index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AddStock model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->add_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionRecieve($id)
    {
        $model = $this->findModel($id);
        $model->status_id = '2';

        if ($model->save()) {
            $inv = Inventory::find()->where(['item_id'=> $model->item_id,'stock_id'=>$model->stock_id])->one();
            $inv->remain = $inv->remain + $model->qty;
            $inv->save();
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AddStock model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AddStock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AddStock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AddStock::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
