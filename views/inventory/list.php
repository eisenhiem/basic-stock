<?php

use yii\bootstrap4\Html;
use kartik\grid\GridView;
use kartik\icons\Icon;
Icon::map($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\InventorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
        
$this->title = $stock->stock_name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-list">

    <?= $this->render('_search_list', ['model' => $searchModel,'id'=>$stock->stock_id]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => "รายการเวชภัณฑ์มิใช่ยา",
            'before' => ' ',
            'type' => \kartik\grid\GridView::TYPE_SUCCESS,
        ],
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'item_id',
                'headerOptions' => ['style' => 'width:40%'],
                'value' => function($model){
                    return $model->item->item_name;
                }
            ],
            [
                'attribute' => 'remain',
                'contentOptions'=> ['style'=>'text-align:center;'],
                'value' => function($model){
                    return number_format($model->remain);
                }
            ],
            [
                'attribute' => 'user_id',
                'contentOptions'=> ['style'=>'text-align:center;'],
                'value' => function($model){
                    return $model->user->fullname;
                }
            ],
            [
                'attribute' => 'd_update',
                'contentOptions'=> ['style'=>'text-align:center;'],
                'value' => function($model){
                    return $model->getThaiDate($model->d_update);
                }
            ],
        ],
    ]); ?>

</div>