<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use app\models\Stock;
use yii\helpers\ArrayHelper;
use kartik\icons\Icon;
Icon::map($this);
$stock = ArrayHelper::map(Stock::find()->all(), 'stock_id', 'stock_name');
/* @var $this yii\web\View */
/* @var $model app\models\InventorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inventory-search">

    <?php $form = ActiveForm::begin([
        'action' => ['list','id'=>$id],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'item_name')->textInput(['placeholder' => 'ค้นหาด้วยชื่อ...'])->label(false) ?>
        </div>
        <div class="col-md-4">
            <?= Html::submitButton(Icon::show('fa fa-search').' ค้นหา', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php //= $form->field($model, 'remain') 
    ?>

    <?php ActiveForm::end(); ?>
    
</div>