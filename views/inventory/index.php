<?php

use yii\bootstrap4\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\icons\Icon;
Icon::map($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\InventorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการคงคลัง';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-index">

    <p>
        <?= Yii::$app->user->identity->role == 1 ? Html::a('เพิ่มรายการ', ['item/create'], ['class' => 'btn btn-success']):'' ?>
    </p>

    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => "รายการเวชภัณฑ์มิใช่ยา",
            'before' => ' ',
            'type' => \kartik\grid\GridView::TYPE_SUCCESS,
        ],
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'รับเข้าคลัง',
                'options' => ['style' => 'width:50px;'],
                'buttonOptions' => ['class' => 'btn btn-primary btn-sm'],
                'template' => '{add}',
                'buttons' => [
                    'add' => function ($url, $model, $key) {
                        return Yii::$app->user->identity->role < 3 ? Html::a(Icon::show('fa fa-cart-plus'), ['addstock/create', 'inv_id' => $model->inventory_id], ['class' => 'btn btn-success', 'style' => ['width' => '50px']]):'';
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'ขอเบิก',
                'options' => ['style' => 'width:50px;'],
                'buttonOptions' => ['class' => 'btn btn-primary btn-sm'],
                'template' => '{req}',
                'buttons' => [
                    'req' => function ($url, $model, $key) {
                        return Html::a(Icon::show('fa fa-list'), ['req/create', 'item_id' => $model->item_id], ['class' => 'btn btn-primary', 'style' => ['width' => '50px']]);
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'ตัดจ่าย',
                'options' => ['style' => 'width:50px;'],
                'buttonOptions' => ['class' => 'btn btn-primary btn-sm'],
                'template' => '{despend}',
                'buttons' => [
                    'despend' => function ($url, $model, $key) {
                        return Html::a(Icon::show('fa fa-dolly'), ['despend/dispend', 'inv_id' => $model->inventory_id], ['class' => 'btn btn-danger', 'style' => ['width' => '50px']]);
                    }
                ]
            ],
            [
                'attribute' => 'item_id',
                'headerOptions' => ['style' => 'width:30%'],
                'value' => function($model){
                    return $model->item->item_name;
                }
            ],
            [
                'attribute' => 'remain',
//                'headerOptions' => ['style' => 'align:right'],
                'contentOptions'=> ['style'=>'text-align:center;'],
                'value' => function($model){
                    return number_format($model->remain);
                }
                //'format' => 'raw',
            ],
            //'inventory_id',
            //'item.item_name',
            //'stock_id',
            //'remain',
            [
                'attribute' => 'user_id',
                'contentOptions'=> ['style'=>'text-align:center;'],
                'value' => function($model){
                    return $model->user->fullname;
                }
            ],
            [
                'attribute' => 'd_update',
                'contentOptions'=> ['style'=>'text-align:center;'],
                'value' => function($model){
                    return $model->getThaiDate($model->d_update);
                }
            ],
            //'d_update',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
