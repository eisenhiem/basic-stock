<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Req */

$this->title = 'ขอเบิกเวชภัณฑ์มิใช่ยา';
$this->params['breadcrumbs'][] = ['label' => 'ทะเบียนรายการขอเบิก', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="req-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
