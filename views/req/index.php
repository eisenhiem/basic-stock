<?php

use yii\bootstrap4\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\icons\Icon;

use app\models\Inventory;
Icon::map($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reqs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="req-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => "รายการเวชภัณฑ์มิใช่ยา",
            'before' => ' ',
            'type' => \kartik\grid\GridView::TYPE_SUCCESS,
        ],
        // 'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'ตัดจ่าย',
                'options' => ['style' => 'width:50px;'],
                'buttonOptions' => ['class' => 'btn btn-primary btn-sm'],
                'template' => '{despend}',
                'buttons' => [
                    'despend' => function ($url, $model, $key) {
                        return Html::a(Icon::show('fa fa-dolly'), ['despend/create', 'req_id' => $model->req_id], ['class' => 'btn btn-danger', 'style' => ['width' => '50px']]);
                    }
                ]
            ],
            'req_id',
            [
                'attribute' => 'stock_id',
                'headerOptions' => ['style' => 'width:20%'],
                'value' => function($model){
                    return $model->stock->stock_name;
                }
            ],
            [
                'attribute' => 'req_date',
                'contentOptions'=> ['style'=>'text-align:center;'],
                'value' => function($model){
                    return Inventory::getThaiDate($model->req_date);
                }
            ],
            [
                'attribute' => 'item_id',
                'headerOptions' => ['style' => 'width:30%'],
                'value' => function($model){
                    return $model->item->item_name;
                }
            ],
            [
                'attribute' => 'qty',
                'contentOptions'=> ['style'=>'text-align:center;'],
                'value' => function($model){
                    return number_format($model->qty);
                }
            ],
            'status_id',
            [
                'attribute' => 'd_update',
                'contentOptions'=> ['style'=>'text-align:center;'],
                'value' => function($model){
                    return Inventory::getThaiDate($model->d_update);
                }
            ],
            'cause',
        ],
    ]); ?>


</div>
