<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Req */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="req-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'req_date')->widget(
                DatePicker::ClassName(),
                [
                    'name' => 'วันที่ขอเบิก',
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวัน'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]
            );
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'qty')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'cause')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('ส่งรายการคำขอเบิก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>