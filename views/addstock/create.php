<?php

use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AddStock */

$this->title = 'รับเข้าคลัง';
$this->params['breadcrumbs'][] = ['label' => 'ทะเบียนรับเข้าคลัง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="add-stock-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
