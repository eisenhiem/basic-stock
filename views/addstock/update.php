<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AddStock */

$this->title = 'Update Add Stock: ' . $model->add_id;
$this->params['breadcrumbs'][] = ['label' => 'Add Stocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->add_id, 'url' => ['view', 'id' => $model->add_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="add-stock-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
