<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\AddStock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="add-stock-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'add_date')->widget(
                DatePicker::ClassName(),
                [
                    'name' => 'วันที่รับเข้าคลัง',
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวัน'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]
            );
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'qty')->textInput() ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('รับเข้าคลัง', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>