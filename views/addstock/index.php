<?php

use app\models\Inventory;
use yii\bootstrap4\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\icons\Icon;
Icon::map($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\AddStockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ทะเบียนรับเข้าคลัง';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="add-stock-index">

    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => "ทะเบียนรับเข้าคลัง",
            'before' => ' ',
            'type' => \kartik\grid\GridView::TYPE_SUCCESS,
        ],
        //'filterModel' => $searchModel,
        'columns' => [
            'add_id',
            [
                'attribute' => 'stock_id',
                'headerOptions' => ['style' => 'width:20%'],
                'value' => function($model){
                    return $model->stock->stock_name;
                }
            ],
            [
                'attribute' => 'item_id',
                'headerOptions' => ['style' => 'width:30%'],
                'value' => function($model){
                    return $model->item->item_name;
                }
            ],
            [
                'attribute' => 'qty',
                'contentOptions'=> ['style'=>'text-align:center;'],
                'value' => function($model){
                    return number_format($model->qty);
                }
            ],
            [
                'attribute' => 'user_id',
                'contentOptions'=> ['style'=>'text-align:center;'],
                'value' => function($model){
                    return $model->user->fullname;
                }
            ],
            [
                'attribute' => 'add_date',
                'contentOptions'=> ['style'=>'text-align:center;'],
                'value' => function($model){
                    return Inventory::getThaiDate($model->add_date);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'สถานะ',
                'options' => ['style' => 'width:50px;'],
                'buttonOptions' => ['class' => 'btn btn-primary btn-sm'],
                'template' => '{recieve}',
                'buttons' => [
                    'recieve' => function ($url, $model, $key) {
                        return $model->status_id == 1 ? Html::a(Icon::show('fa fa-cart-plus'), ['addstock/recieve', 'id' => $model->add_id], ['class' => 'btn btn-success', 'style' => ['width' => '50px']]):'&#x2714;';
                    }
                ]
            ],
        ],
    ]); ?>


</div>
