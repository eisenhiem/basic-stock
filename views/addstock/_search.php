<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\icons\Icon;
Icon::map($this);

/* @var $this yii\web\View */
/* @var $model app\models\InventorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="add-stock-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'item_name')->textInput(['placeholder' => 'ค้นหาด้วยชื่อ...'])->label(false) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'add_date')->widget(
                DatePicker::ClassName(),
                [
                    'name' => 'วันที่รับเข้าคลัง',
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ค้นหาจากวันที่รับเข้าคลัง'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]
            )->label(false);
            ?>
        </div>
        <div class="col-md-4">
            <?= Html::submitButton(Icon::show('fa fa-search').' ค้นหา', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php //= $form->field($model, 'remain') 
    ?>

    <?php ActiveForm::end(); ?>
    
</div>