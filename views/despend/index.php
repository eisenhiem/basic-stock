<?php

use yii\bootstrap4\Html;
use kartik\grid\GridView;
use app\models\Inventory;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DespendSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ทะเบียนการตัดจ่าย';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="despend-index">

    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => "รายการตัดจ่าย",
            'before' => ' ',
            'type' => \kartik\grid\GridView::TYPE_DANGER,
        ],
        //'filterModel' => $searchModel,
        'columns' => [
            'despend_id',
            [
                'attribute' => 'stock_id',
                'headerOptions' => ['style' => 'width:20%'],
                'value' => function($model){
                    return $model->stock->stock_name;
                }
            ],
            [
                'attribute' => 'item_id',
                'headerOptions' => ['style' => 'width:30%'],
                'value' => function($model){
                    return $model->item->item_name;
                }
            ],
            [
                'attribute' => 'qty',
                'contentOptions'=> ['style'=>'text-align:center;'],
                'value' => function($model){
                    return number_format($model->qty);
                }
            ],
            [
                'attribute' => 'to_stock',
                'contentOptions'=> ['style'=>'text-align:center;'],
                'value' => function($model){
                    return $model->getToStockName();
                }
            ],
            [
                'attribute' => 'despend_date',
                'contentOptions'=> ['style'=>'text-align:center;'],
                'value' => function($model){
                    return Inventory::getThaiDate($model->despend_date);
                }
            ],
        ],
    ]); ?>

</div>
