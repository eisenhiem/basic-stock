<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Despend */

$this->title = 'ตัดจ่ายใบคำขอเลขที่ '.$model->req_id;
$this->params['breadcrumbs'][] = ['label' => 'รายการคำขอเบิก', 'url' => ['req/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="despend-create">

<?= $this->render('_form', [
        'model' => $model,
        'inv' => $inv,
    ]) ?>

</div>
