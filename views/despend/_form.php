<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use kartik\date\DatePicker;

$status = ['2' => 'ตัดจ่ายครบถ้วน', '4' => 'ตัดจ่ายบางส่วน'];
/* @var $this yii\web\View */
/* @var $model app\models\Despend */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="despend-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'despend_date')->widget(
                DatePicker::ClassName(),
                [
                    'name' => 'วันที่ตัดจ่าย',
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวัน'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]
            );
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'qty')->textInput()->label('จำนวน (คงเหลือในคลัง '.$inv->remain.')') ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('ตัดจ่าย', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>