<?php

use app\models\Stock;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\icons\Icon;
Icon::map($this);

$stock = ArrayHelper::map(Stock::find()->all(),'stock_id','stock_name');
/* @var $this yii\web\View */
/* @var $model app\models\DespendSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="despend-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'item_name')->textInput(['placeholder' => 'ค้นหาด้วยชื่อ...'])->label(false) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'despend_date')->widget(
                DatePicker::ClassName(),
                [
                    'name' => 'วันที่ตัดจ่าย',
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวันที่ตัดจ่าย'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]
            )->label(false);
            ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'to_stock')->dropdownList($stock,['prompt' => 'ค้นหาจากคลังที่ตัดจ่ายไปให้'])->label(false) ?>
        </div>
        <div class="col-md-3">
            <?= Html::submitButton(Icon::show('fa fa-search').' ค้นหา', ['class' => 'btn btn-primary']) ?>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>