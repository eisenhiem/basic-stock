<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Despend */

$this->title = 'ตัดจ่ายภายในหน่วย: ' . $inv->item->item_name;
$this->params['breadcrumbs'][] = ['label' => 'รายการคงคลัง', 'url' => ['inventory/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="despend-update">

    <?= $this->render('_form', [
        'model' => $model,
        'inv' => $inv,
    ]) ?>

</div>
