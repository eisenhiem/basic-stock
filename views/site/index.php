<?php

use yii\bootstrap4\Html;

/* @var $this yii\web\View */

$this->title = 'ระบบบริหารคลังเวชภัณฑ์มิใช่ยา';
?>
<div class="site-index">
    <div class="row">
        <?php
        if (!Yii::$app->user->isGuest) {
        ?>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                <div class="card text-center border-0">
                    <p align="center">
                        <?= Html::a(
                            "<img class='card-img-top' src='../web/images/warehouse.jpg' alt='Card image cap'><br>" . 'บริหารการคลัง',
                            ['inventory/index'],
                            ['class' => 'btn btn-lg btn-outline-primary', 'style' => 'border-radius: 20px']
                        )
                        ?>
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                <div class="card text-center border-0">
                    <p align="center">
                        <?= Html::a(
                            "<img class='card-img-top' src='../web/images/add.jpg' alt='Card image cap'><br>" . 'ขอเบิก',
                            ['req/index'],
                            ['class' => 'btn btn-lg btn-outline-primary', 'style' => 'border-radius: 20px']
                        )
                        ?>
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                <div class="card text-center border-0">
                    <p align="center">
                        <?= Html::a(
                            "<img class='card-img-top' src='../web/images/req.jpg' alt='Card image cap'><br>" . 'รับเข้าคลัง',
                            ['addstock/index'],
                            ['class' => 'btn btn-lg btn-outline-primary', 'style' => 'border-radius: 20px']
                        )
                        ?>
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                <div class="card text-center border-0">
                    <p align="center">
                        <?= Html::a(
                            "<img class='card-img-top' src='../web/images/despend.jpg' alt='Card image cap'><br>" . 'จ่ายออกจากคลัง',
                            ['despend/index'],
                            ['class' => 'btn btn-lg btn-outline-primary', 'style' => 'border-radius: 20px']
                        )
                        ?>
                    </p>
                </div>
            </div>
            <?php
            if (Yii::$app->user->identity->role == 1) {
            ?>
                <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                    <div class="card text-center border-0">
                        <p align="center">
                            <?= Html::a(
                                "<img class='card-img-top' src='../web/images/setting.jpg' alt='Card image cap'><br>" . 'ตั้งค่าคลัง',
                                ['stock/index'],
                                ['class' => 'btn btn-lg btn-outline-primary', 'style' => 'border-radius: 20px']
                            )
                            ?>
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                    <div class="card text-center border-0">
                        <p align="center">
                            <?= Html::a(
                                "<img class='card-img-top' src='../web/images/user.jpg' alt='Card image cap'><br>" . 'จัการผู้ใช้',
                                ['user/admin/index'],
                                ['class' => 'btn btn-lg btn-outline-primary', 'style' => 'border-radius: 20px']
                            )
                            ?>
                        </p>
                    </div>
                </div>
                <?php

                foreach ($stock as $s) {
                ?>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="card text-center border-0">
                            <p align="center">
                                <?= Html::a(
                                    "<img class='card-img-top' src='../web/images/substock.jpg' alt='Card image cap'><br>" . $s->stock_name,
                                    ['inventory/list', 'id' => $s->stock_id],
                                    ['class' => 'btn btn-lg btn-outline-primary', 'style' => 'border-radius: 20px']
                                )
                                ?>
                            </p>
                        </div>
                    </div>
            <?php
                }
            }
            ?>
        <?php
        }
        if (Yii::$app->user->isGuest) {
        ?>
            <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                <div class="card text-center border-0">
                    <p align="center">
                        <?= Html::a(
                            "<img class='card-img-top' src='../web/images/login.jpg' alt='Card image cap'><br>" . 'เข้าระบบ',
                            ['user/security/login'],
                            ['class' => 'btn btn-lg btn-outline-primary', 'style' => 'border-radius: 20px']
                        )
                        ?>
                    </p>
                </div>
            </div>
        <?php } ?>
    </div>
</div>