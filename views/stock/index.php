<?php

use yii\bootstrap4\Html;
use kartik\grid\GridView;
use kartik\icons\Icon;
Icon::map($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\StockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ทะเบียนคลัง';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stock-index">

    <p>
        <?= Html::a('เพิ่มคลัง', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' => [
            'heading' => "รายการเวชภัณฑ์มิใช่ยา",
            'type' => \kartik\grid\GridView::TYPE_SUCCESS,
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'stock_name',
            'stock_manager',
            [
                'attribute' => 'stock_level',
                'value' => function($model){
                    return $model->getLevelName();
                }
            ],
            [
                'header' => 'สถานะ',
                'value' => function ($model) {
                    if ($model->is_active == '0') {
                        return Html::a('ปิดคลัง', ['change', 'id' => $model->stock_id], [
                            'class' => 'btn btn-md btn-danger btn-block',
                            'data-method' => 'post',
                        ]);
                    } else {
                        return Html::a('เปิดใช้งาน', ['change', 'id' => $model->stock_id], [
                            'class' => 'btn btn-md btn-success btn-block',
                            'data-method' => 'post',
                        ]);
                    }
                },
                'format' => 'raw',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'แก้ไข',
                'options' => ['style' => 'width:50px;'],
                'buttonOptions' => ['class' => 'btn btn-primary btn-sm'],
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Yii::$app->user->identity->role < 3 ? Html::a(Icon::show('fa fa-pencil-alt'), ['update', 'id' => $model->stock_id], ['class' => 'btn btn-warning', 'style' => ['width' => '50px']]):'';
                    }
                ]
            ],
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
