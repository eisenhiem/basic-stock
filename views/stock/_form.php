<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Stock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stock-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'stock_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'stock_manager')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'stock_level')->dropDownList($model->getItemLevel()) ?>

    <?= $form->field($model, 'is_active')->radioList($model->getItemStatus()) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
